import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Properties;

public class TestAddApp {

    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver","/home/posa/Downloads/chromedriver_linux64/chromedriver");


        ChromeDriver driver = new ChromeDriver();

        //Lansare site
        driver.get("https://posabogdan.tech/demo/selenium/add.html");

        System.out.println(driver.getTitle());

        driver.findElement(By.id("nr1")).sendKeys("1");
        driver.findElement(By.id("nr2")).sendKeys("2");

        driver.findElement(By.id("adunare")).submit();

        int result = Integer.parseInt(driver.findElement(By.id("result")).getText());

        System.out.println(result);

    }
}
