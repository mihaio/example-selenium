import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestAddApp {

    @Test
    public void testGoodAddApp() {
        runTests("https://posabogdan.tech/demo/selenium/add.html");
    }

    @Test
    public void testBuggyAddApp() {
        runTests("https://posabogdan.tech/demo/selenium/add_bug.html");
    }


    private void runTests(String url) {
        System.setProperty("webdriver.chrome.driver", "/home/posa/Downloads/chromedriver_linux64/chromedriver");

        ChromeDriver driver = new ChromeDriver();

        //Lansare site
        driver.get(url);

        System.out.println(driver.getTitle());

        for (int i = 0; i < 100; i++) {
            int nr1 = i;
            int nr2 = 2;
            driver.findElement(By.id("nr1")).clear();
            driver.findElement(By.id("nr2")).clear();
            driver.findElement(By.id("nr1")).sendKeys(nr1 + "");
            driver.findElement(By.id("nr2")).sendKeys(nr2 + "");
            driver.findElement(By.id("adunare")).submit();

            int result = Integer.parseInt(driver.findElement(By.id("result")).getText());
            Assert.assertEquals(nr1 + nr2, result);
        }
    }


}
